package com.example.uttam.tablelayoutdynamically;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;

public class MainActivity extends AppCompatActivity {

    TableLayout tableLayout;
    TableRow tableRow1,tableRow2;
    Button b1,b2,b3,b4,b5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tableLayout=new TableLayout(this);

        //creating oblect for TableLayout, TableRow & Button
        tableLayout=new TableLayout(this);
        tableRow1=new TableRow(this);
        tableRow2=new TableRow(this);
        b1=new Button(this);
        b2=new Button(this);
        b3=new Button(this);
        b4=new Button(this);
        b5=new Button(this);

        //Parameters of TableLayout
        TableLayout.LayoutParams lp=new TableLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        tableLayout.setLayoutParams(lp);

        //Setting the Text of Button
        b1.setText("Button 1");
        b2.setText("Button 2");
        b3.setText("Button 3");
        b4.setText("Button 4");
        b5.setText("Button 5");

        //Add Button to the TableRow
        tableRow1.addView(b1);
        tableRow1.addView(b2);
        tableRow1.addView(b3);
        tableRow2.addView(b4);
        tableRow2.addView(b5);

        //Setting TableRow to TableLayout
        tableLayout.addView(tableRow1);
        tableLayout.addView(tableRow2);

        //adding the view
        //display dynamic table without activity_main file
        //setContentView(tableLayout);

        //display the activity_main file
        setContentView(R.layout.activity_main);

        //display the table_layout file
        //setContentView(R.layout.table_layout);
    }
}
